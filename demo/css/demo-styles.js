import { setDocumentCustomStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { css } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }

  h3 {
    margin: 0;
    margin-top: 20px;
    text-align: center;
  }

  .content-buttons {
    display: flex;
    align-items: center;
    width: calc(100% - 30px);
    justify-content: center;
    padding: 15px;
  }

  #result {
    margin-left: 15px;
    margin-right: 15px;
    border-top: 1px solid #e1e1e1;
    width: calc(100% - 60px);
    padding: 15px;
  }

  .content-buttons * {
    margin: 5px;
  }

  .url-request {
    width: calc(100% - 250px);
    height: 47px;
    border: 1px solid #BDBDBD;
    outline: none;
    font-family: sans-serif;
    padding: 5px 10px;
    font-size: 1em;
  }

  button {
    border: none;
    color: white;
    padding: 15px 22px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor: pointer;
  }

  button:disabled,
button[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
  opacity: 0.6;
  padding: 15px 22px;
}

  .success {
    background-color: #4caf50;
  } /* Green */
  .blue {
    background-color: #008cba;
  } /* Blue */
  .danger {
    background-color: #f44336;
  } /* Red */
  .gray {
    background-color: #e7e7e7;
    color: black;
  } /* Gray */
  .dark {
    background-color: #555555;
  } /* Black */


  @media screen and (max-width: 480px) {
    .content-buttons {
      flex-direction: column;
    }
    .url-request {
      width: 100%; 
    }
    button {
      width: 100%;
    }
  }

`);
